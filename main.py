"""
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuch

@author: Henrik Buechs
"""

from functions import m_json
from functions import m_pck
#m_pck.check_sensors()

# metadaten erstellen
#path_setup = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
path_setup = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
metadata = m_json.get_metadata_from_setup(path_setup)

# serials der Sensoren den metadaten hinzufügen
path_datasheets = "/home/pi/calorimetry_home/datasheets"
m_json.add_temperature_sensor_serials(path_datasheets, metadata)

# Messung durchführen
data = m_pck.get_meas_data_calorimetry(metadata)

# Messdaten speichern und archivieren
path_data = "/home/pi/calorimetry_home/data/newton"
m_pck.logging_calorimetry(data, metadata, path_data, path_datasheets)

m_json.archiv_json(path_datasheets, path_setup, path_data)

